/**
 * Created by karan on 27-Feb-15.
 */
var express = require('express');
var router = express.Router();
var commonFunc=require('./commonFunction');
var sendResponse = require('./sendResponse');
var async = require('async');
var request = require('request');
var md5 = require('MD5');

//get the API of user registration and store the request data to DB and also generate the corresponding response
router.post('/register_user',function(req,res){
    var userMail = req.body.email;
    var firstName = req.body.first_name;
    var lastName = req.body.last_name;

    var password = req.body.password;
    var deviceType = req.body.device_type; // 0 for android, 1 for ios, 2 for windows
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;

    var sub ="Successfully Registered";
    var message ="WellCome To test sending...";
    var manValues = [userMail,firstName,lastName,password,deviceType,deviceToken,appVersion];
    async.waterfall([
            function (callback) {
                commonFunc.checkBlank(res, manValues, callback);
            },
            function (callback) {
                checkEmailAvailability(res, userMail, callback);
            },
            function (callback){
                checkValidMail(res,userMail,callback);
            }],function(err,resultCallback){
            commonFunc.sendEmail(userMail, sub, message, function (result) {
                    if (result == 1) {
                        var loginTime = new Date();
                        var accessToken = md5(userMail + password + loginTime);
                        var encryptPassword = md5(password);
                        var sql = "INSERT INTO `userinfo`(`uName`, `userLNmae`, `uMail`, `uPass`,`accessToken`,`userDeviceType`,`userDeviceToken`,`userAppV`) VALUES (?,?,?,?,?,?,?,?)";
                        var values = [firstName, lastName, userMail, encryptPassword, accessToken, deviceType, deviceToken, appVersion];
                        dbConnection.Query(res, sql, values, function (userInsertResult) {

                            var data = {
                                access_token: accessToken,
                                first_name: firstName,
                                last_name: lastName
                            };
                            sendResponse.sendSuccessData(data, res);
                        });
                        //sendResponse.successStatusMsg(res);
                        console.log("Mail Sent");
                    }
                    else {
                        sendResponse.wrongMailId(res);
                        console.log(userMail)
                        console.log("Error Sending Mail");
                        //res.send("Error Sending Mail");
                    }
                })
            }
    );

});

//check email id is Exist On SMTP Server or not
function checkValidMail(ress,userMail,callback){
    request({
        url: "https://www.emailitin.com/email_validator",
        method: "POST",
        form: {email: userMail},
        json: true
    }, function (err,res, body) {
        //console.log(body.valid);
        if (body.valid) {
            callback (null);
        }
        else {
            sendResponse.wrongMailId(ress);
            console.log(userMail)
            console.log("Email not Exist On SMTP Server");
        }
    });
}
//check the request mail id already registered or not.
function checkEmailAvailability(res,email,callback){
    var sql = "SELECT `userId` FROM `userinfo` WHERE `uMail`=? limit 1";
    var values = [email];

    dbConnection.Query(res, sql, values, function (userResponse) {

        if (userResponse.length) {
            // var errorMsg = 'Email already exists!';
            sendResponse.sendErrorMessage(constant.responseMessage.EMAIL_EXISTS, res);
        }
        else {
            //res.send("Okay");
            callback();
        }
    });
}

//get a logIn API
router.post('/userLogin',function(req,res){
    var userMail = req.body.email;
    var password = req.body.password;
    var deviceType = req.body.device_type; // 0 for android, 1 for ios, 2 for windows
    var deviceToken = req.body.device_token;
    var appVersion = req.body.app_version;
    var manValues = [userMail,password,deviceType,deviceToken,appVersion];
    async.waterfall([
        function (callback) {
            commonFunc.checkBlank(res, manValues, callback);
        }],function(err,resultCallback) {
            var encryptPassword = md5(password);
            var sql = "SELECT `userId` FROM `userinfo` WHERE `uMail`=? limit 1";
            dbConnection.Query(res,sql, [userMail] , function (userResponse) {
                if(userResponse.length==0){
                    var errMsg="your email has not exist. Please sign up to login";
                    sendResponse.sendErrorMessage(errMsg, res);
                }
                else{
                    var userId=userResponse[0].userId;
                    var sql = "SELECT accessToken FROM `userinfo` WHERE `userId`=? && `uPass`=? limit 1";
                    dbConnection.Query(res,sql, [userId,encryptPassword] , function (userResponse1) {
                        if (userResponse1.length == 0) {
                            var errMsg="Password is incorrect";
                            sendResponse.sendErrorMessage(errMsg, res);
                        }
                        else {
                            var loginTime = new Date();
                            var accessToken = md5(userMail + password + loginTime);
                            var sql = "UPDATE userinfo set accessToken=?, loginStatus=? WHERE userId=?";
                            dbConnection.Query(res,sql, [accessToken,1,userId] , function (userResponse2) {
                                var sql = "SELECT accessToken,uName,userLNmae,uMail FROM `userinfo` WHERE `userId`=? limit 1";
                                dbConnection.Query(res,sql, [userId] , function (userResponse2) {
                                    var data = {
                                        access_token: userResponse2[0].accessToken,
                                        userId: userId,
                                        first_name: userResponse2[0].uName,
                                        last_name: userResponse2[0].userLNmae,
                                        emailId: userResponse2[0].uMail
                                    };
                                    sendResponse.sendSuccessData(data, res);
                                });
                                //res.send("errrrrr");
                            });
                        }
                    })

                }
            });

        });


});

//logOut API
router.post('/logout_user',function(req,res) {
    var accessToken=req.body.access_token;
    var sql = "UPDATE userinfo set loginStatus=? where accessToken=?";
    dbConnection.Query(res,sql, [0,accessToken] , function (userResponse2) {
        if(userResponse2.affectedRows==0) {
            var loutMsg = "Invalid Access Token";
            sendResponse.sendSuccessData(loutMsg, res);
        }
        else {
            var loutMsg = "Successfully Logout";
            sendResponse.sendSuccessData(loutMsg, res);
        }
    });
});
module.exports = router;