/**
 * Created by karan on 27-Feb-15.
 */
var mailConfig=config.get('emailSettings');
var sendResponse = require('./sendResponse');

//Definition of send mail that is used to send a mail with the help of nodemailer module
exports.sendEmail = function(receiverMailId, subject,message, callback) {
    var nodeMailer = require("nodemailer");
    var smtpTransport = nodeMailer.createTransport("SMTP", {
        service: 'Gmail',
        auth: {
            user: mailConfig.email,
            pass: mailConfig.password
        }
    });

    var mailOptions = {
        from: mailConfig.email,
        to: receiverMailId,
        subject: subject,
        text: 'WellCome To ------',
        html: '<b>'+message+'</b>'

    }
    smtpTransport.sendMail(mailOptions, function(error, response) {
        if (error) {
            console.log(error);
            return callback(0);
        } else {
            //  console.log('hi');
            return callback(1);
        }
    });
};

//definition of checkBlank that calling from user.js file
exports.checkBlank = function (res, manValues, callback) {

    var checkBlankData = checkBlank(manValues);

    if (checkBlankData) {
        sendResponse.parameterMissingError(res);
    }
    else {
        callback(null);
    }
}


//check passing parameter is blank or not
function checkBlank(arr) {

    var arrLength = arr.length;

    for (var i = 0; i < arrLength; i++) {
        if ((arr[i] == '')||(arr[i] == undefined)||(arr[i] == '(null)')) {
            return 1;
            break;
        }
    }
    return 0;
}