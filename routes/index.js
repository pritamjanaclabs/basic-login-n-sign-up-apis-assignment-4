/**
 * Created by karan on 27-Feb-15.
 */
var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('index', { title: 'User Access' });
});
/*GET login page*/
router.get('/login_user', function(req, res, next) {
    res.render('login');
})
/* GET registration page. */
router.get('/register', function(req, res, next) {
    res.render('test');
});

module.exports = router;