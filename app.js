process.env.NODE_ENV = 'test';
config = require('config');
constant = require('./routes/constant');
dbConnection = require('./routes/dbConnection');
var express = require('express');


var path = require('path');
var bodyParser = require('body-parser');
var http = require('http');

var routes = require('./routes/index');
var users = require('./routes/users');

var app = express();
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');
app.set('json spaces', 1);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));

app.use(express.static(path.join(__dirname, 'public')));

app.get('/register', routes);
app.get('/', routes);
app.get('/login_user', routes);
app.post('/userLogin', users);
app.post('/register_user', users);
app.post('/logout_user', users);

app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});
http.createServer(app).listen(config.get('PORT'), function () {
    console.log("Express server listening on port " + config.get('PORT'));
});